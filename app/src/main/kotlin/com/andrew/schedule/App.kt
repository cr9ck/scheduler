package com.andrew.schedule

import android.app.Application
import com.andrew.schedule.di.component.AppComponentProvider

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        AppComponentProvider.provideAppComponent(this)
    }
}