package com.andrew.schedule.network

import com.andrew.schedule.model.data.DataList
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface PrayerApi {
    companion object {
        const val ENDPOINT = "http://api.aladhan.com/v1/"
        const val GET_DATA_BY_CITY = "calendar"
    }

    @GET(GET_DATA_BY_CITY)
    fun getDataByLocation(
        @Query(value = "latitude") latitude: String,
        @Query(value = "longitude") longitude: String,
        @Query(value = "month") month: Int,
        @Query(value = "year") year: Int
    ): Call<DataList>
}