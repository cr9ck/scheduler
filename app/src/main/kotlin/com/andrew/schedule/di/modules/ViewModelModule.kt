package com.andrew.schedule.di.modules

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.andrew.base.ViewModelFactory
import com.andrew.schedule.di.ViewModelKey
import com.andrew.schedule.presentation.home.HomeViewModel
import com.andrew.schedule.presentation.splash.SplashViewModel
import com.andrew.schedule.presentation.timing.TimingViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import kotlinx.coroutines.InternalCoroutinesApi

@Module
abstract class ViewModelModule {

    @Binds
    abstract fun bindViewModelFactory(viewModelFactory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(SplashViewModel::class)
    abstract fun bindSplashViewModel(viewModel: SplashViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(HomeViewModel::class)
    abstract fun bindHomeViewModel(viewModel: HomeViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(TimingViewModel::class)
    abstract fun bindTimingViewModel(viewModel: TimingViewModel): ViewModel
}