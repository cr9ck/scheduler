package com.andrew.schedule.di.component

import com.andrew.schedule.App
import com.andrew.schedule.di.AppScope
import com.andrew.schedule.di.modules.AppModule
import com.andrew.schedule.di.modules.NetworkModule
import com.andrew.schedule.di.modules.RepositoryModule
import com.andrew.schedule.di.modules.ViewModelModule
import com.andrew.schedule.presentation.MainActivity
import com.andrew.schedule.presentation.home.HomeFragment
import com.andrew.schedule.presentation.splash.SplashScreenFragment
import com.andrew.schedule.presentation.timing.TimingFragment
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import kotlinx.coroutines.InternalCoroutinesApi

@Component(
    modules = [
        AndroidInjectionModule::class,
        NetworkModule::class,
        ViewModelModule::class,
        AppModule::class,
        RepositoryModule::class]
)
@AppScope
interface AppComponent {

    @InternalCoroutinesApi
    fun inject(fragment: HomeFragment)
    fun inject(fragment: SplashScreenFragment)
    fun inject(fragment: TimingFragment)
    fun inject(activity: MainActivity)

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun withContext(app: App): Builder

        fun build(): AppComponent
    }
}