package com.andrew.schedule.di.modules

import com.andrew.schedule.di.AppScope
import com.andrew.schedule.model.db.DataDao
import com.andrew.schedule.model.mapper.MonthMapper
import com.andrew.schedule.model.mapper.WeekdayMapper
import com.andrew.schedule.model.repository.DataRepository
import com.andrew.schedule.model.repository.LocationRepository
import com.andrew.schedule.network.PrayerApi
import dagger.Module
import dagger.Provides

@Module
class RepositoryModule {

    @Provides
    @AppScope
    fun provideLocationRepository(prayerApi: PrayerApi): LocationRepository = LocationRepository(prayerApi)

    @Provides
    @AppScope
    fun provideDataRepository(dataDao: DataDao) = DataRepository(dataDao, WeekdayMapper(), MonthMapper())
}