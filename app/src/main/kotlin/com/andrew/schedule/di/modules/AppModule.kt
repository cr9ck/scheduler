package com.andrew.schedule.di.modules

import android.content.Context
import com.andrew.schedule.App
import com.andrew.schedule.di.AppScope
import com.andrew.schedule.model.AppPreference
import com.andrew.schedule.model.db.AppDatabase
import com.andrew.schedule.network.PrayerApi
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.SettingsClient
import dagger.Module
import dagger.Provides

@Module
class AppModule {

    @Provides
    @AppScope
    fun provideBaseUrl(): String {
        return PrayerApi.ENDPOINT
    }

    @Provides
    @AppScope
    fun provideContext(app: App): Context = app

    @Provides
    @AppScope
    fun provideAddDatabase(context: Context) =
        AppDatabase.getInstance(context)

    @Provides
    @AppScope
    fun provideDataDao(database: AppDatabase) =
        database.dataDao()

    @Provides
    @AppScope
    fun provideFusedLocationProviderClient(context: Context): FusedLocationProviderClient =
        LocationServices.getFusedLocationProviderClient(context)

    @Provides
    @AppScope
    fun provideSettingsClient(context: Context): SettingsClient =
        LocationServices.getSettingsClient(context)

    @Provides
    @AppScope
    fun provideAppPreference(context: Context) = AppPreference(context)
}