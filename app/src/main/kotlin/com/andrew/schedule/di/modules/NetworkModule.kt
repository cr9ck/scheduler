package com.andrew.schedule.di.modules

import com.andrew.schedule.di.AppScope
import com.andrew.schedule.network.PrayerApi
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@Module
class NetworkModule {

    @Provides
    @AppScope
    fun provideNetworkService(baseUrl: String): Retrofit = Retrofit.Builder()
            .baseUrl(baseUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

    @Provides
    fun providePrayerApi(retrofit: Retrofit): PrayerApi = retrofit.create(PrayerApi::class.java)
}