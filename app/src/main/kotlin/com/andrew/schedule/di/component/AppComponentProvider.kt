package com.andrew.schedule.di.component

import com.andrew.schedule.App

object AppComponentProvider {

    lateinit var appComponent: AppComponent

    fun provideAppComponent(app: App): AppComponent {
        appComponent = DaggerAppComponent.builder().withContext(app).build()
        return appComponent
    }
}