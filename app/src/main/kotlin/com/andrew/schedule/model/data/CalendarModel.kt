package com.andrew.schedule.model.data

import com.google.gson.annotations.SerializedName

data class CalendarModel (
    @SerializedName(value = "date") val date: String,
    @SerializedName(value = "day") val day: Int,
    @SerializedName(value = "format") val format: String,
    @SerializedName(value = "weekday") val weekday: Weekday,
    @SerializedName(value = "month") val month: Month,
    @SerializedName(value = "holidays") val holidays : List<String>?
)