package com.andrew.schedule.model.db.typeConverters

import androidx.room.TypeConverter

class HolidaysTypeConverter {

    private val splitter = ","

    @TypeConverter
    fun toList(timings: String): List<String> {
        return timings.split(splitter)
    }

    @TypeConverter
    fun toString(timings: List<String>?): String {
        return timings?.joinToString { it }?:""
    }
}