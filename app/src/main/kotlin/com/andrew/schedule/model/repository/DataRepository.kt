package com.andrew.schedule.model.repository

import androidx.room.Transaction
import com.andrew.schedule.model.data.CalendarModel
import com.andrew.schedule.model.data.Data
import com.andrew.schedule.model.data.Date
import com.andrew.schedule.model.data.Meta
import com.andrew.schedule.model.db.DataDao
import com.andrew.schedule.model.db.entity.*
import com.andrew.schedule.model.mapper.MonthMapper
import com.andrew.schedule.model.mapper.WeekdayMapper

class DataRepository constructor(
    private val dataDao: DataDao,
    private val weekdayMapper: WeekdayMapper,
    private val monthMapper: MonthMapper
) {
    fun getTimingByDataId(dataId: Long) = dataDao.getDataEntities(dataId).timings

    suspend fun getDataIdBy(day: Int) = dataDao.getDataIdBy(day)

    suspend fun getCalendarBy(calendarType: CalendarType): List<CalendarEntity> = dataDao.getCalendarBy(calendarType)

    suspend fun insertData(dataList: List<Data>) {
        dropAllTables()
        dataList.forEach { insertDataTransactional(it) }
    }

    @Transaction
    suspend fun dropAllTables() {
        dataDao.deleteData()
    }

    @Transaction
    suspend fun insertDataTransactional(data: Data) {
        val date = data.date
        val meta = data.meta

        val dataId = insertData(data)
        val dateId = insertDate(date, dataId)

        insertMeta(meta, dataId)
        insertCalendar(date.gregorian, dateId, CalendarType.GREGORIAN)
        insertCalendar(date.hijri, dateId, CalendarType.HIJRI)
    }

    private suspend fun insertData(data: Data): Long {
        val timingList = data.timings.map { TimingEntity(it.key, it.value) }.toList()
        return dataDao.insert(DataEntity(0, timingList))
    }

    private suspend fun insertDate(date: Date, dataId: Long) =
        dataDao.insert(
            DateEntity(
                0,
                dataId,
                date.readable,
                date.timestamp
            )
        )

    private suspend fun insertMeta(meta: Meta, dataId: Long) =
        dataDao.insert(
            MetaEntity(
                0,
                dataId,
                meta.latitude,
                meta.longitude,
                meta.timezone
            )
        )

    private suspend fun insertCalendar(calendar: CalendarModel, dateId: Long, calendarType: CalendarType) =
        dataDao.insert(
            CalendarEntity(
                0,
                calendar.date,
                calendar.day,
                calendar.format,
                dateId,
                calendarType,
                weekdayMapper.mapToEntity(calendar.weekday),
                monthMapper.mapToEntity(calendar.month),
                calendar.holidays
            )
        )
}