package com.andrew.schedule.model.data

import com.google.gson.annotations.SerializedName

data class Data(
    @SerializedName(value = "timings")  val timings: Map<String, String>,
    @SerializedName(value = "date")  val date: Date,
    @SerializedName(value = "meta") val meta: Meta
)