package com.andrew.schedule.model.mapper

import com.andrew.schedule.model.data.Weekday
import com.andrew.schedule.model.db.entity.WeekdayEntity
import javax.inject.Inject

class WeekdayMapper @Inject constructor(): Mapper<Weekday, WeekdayEntity> {

    override fun mapToModel(entity: WeekdayEntity) = with(entity) { Weekday(en, ar) }

    override fun mapToEntity(model: Weekday) = with(model) { WeekdayEntity(en, ar) }

    override fun mapToModel(entity: List<WeekdayEntity>) = entity.map { mapToModel(it) }

    override fun mapToEntity(model: List<Weekday>) = model.map { mapToEntity(it) }
}