package com.andrew.schedule.model.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.andrew.schedule.model.db.entity.*

@Dao
interface DataDao {

    @Query(value = "SELECT * FROM ${DataEntity.TABLE_NAME}")
    fun getData(): List<DataEntity>

    @Query(value = "SELECT * FROM ${DateEntity.TABLE_NAME} WHERE id=:id")
    fun getDate(id: Long): DateEntity

    @Query(value = "SELECT * FROM ${CalendarEntity.TABLE_NAME} WHERE type=:calendarType ORDER BY day")
    fun getCalendarBy(calendarType: CalendarType): List<CalendarEntity>

    @Query(value = "SELECT * FROM ${MetaEntity.TABLE_NAME} WHERE id=:id")
    fun getMeta(id: Long): MetaEntity

    @Query(value = "SELECT DE.id FROM ${DataEntity.TABLE_NAME} AS DE" +
            " WHERE DE.id=" +
            " (" +
            " SELECT D.dataId FROM ${DateEntity.TABLE_NAME} AS D " +
            " WHERE D.id=" +
            " (" +
            " SELECT C.dateID FROM ${CalendarEntity.TABLE_NAME} AS C" +
            " WHERE C.day=:day" +
            ")" +
            ")")
    fun getDataIdBy(day: Int): Long

    @Query("SELECT * FROM ${DataEntity.TABLE_NAME} WHERE id =:dataId")
    fun getDataEntities(dataId: Long): DataEntity

    @Insert
    suspend fun insert(dataEntity: DataEntity): Long

    @Insert
    suspend fun insert(calendarEntity: CalendarEntity): Long

    @Insert
    suspend fun insert(dateEntity: DateEntity): Long

    @Insert
    suspend fun insert(metaEntity: MetaEntity): Long

    @Query("DELETE FROM ${DataEntity.TABLE_NAME}")
    suspend fun deleteData()
}

