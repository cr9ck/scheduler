package com.andrew.schedule.model.db.entity

data class WeekdayEntity (
    val en : String,
    val ar : String?
)