package com.andrew.schedule.model.db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity(tableName = MetaEntity.TABLE_NAME,
    foreignKeys = [
        ForeignKey(
            entity = DataEntity::class,
            parentColumns = ["id"],
            childColumns = ["dataId"],
            onDelete = ForeignKey.CASCADE
        )]
)
data class MetaEntity(
    @PrimaryKey(autoGenerate = true) val id: Long,
    @ColumnInfo(name = "dataId") val dataId: Long,
    @ColumnInfo(name = "latitude") val latitude: Double,
    @ColumnInfo(name = "longitude") val longitude: Double,
    @ColumnInfo(name = "timezone") val timezone: String
) {
    companion object {
        const val TABLE_NAME = "Meta"
    }
}