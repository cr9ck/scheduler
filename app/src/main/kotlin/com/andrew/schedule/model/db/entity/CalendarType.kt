package com.andrew.schedule.model.db.entity

enum class CalendarType {
    GREGORIAN,
    HIJRI
}