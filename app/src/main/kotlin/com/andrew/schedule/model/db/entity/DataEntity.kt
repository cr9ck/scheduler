package com.andrew.schedule.model.db.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = DataEntity.TABLE_NAME)
data class DataEntity(
    @PrimaryKey(autoGenerate = true) val id: Long,
    val timings: List<TimingEntity>) {
    companion object {
        const val TABLE_NAME = "Data"
    }
}