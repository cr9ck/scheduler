package com.andrew.schedule.model.db.entity

import androidx.room.*

@Entity(tableName = CalendarEntity.TABLE_NAME,
    foreignKeys = [
        ForeignKey(
            entity = DateEntity::class,
            parentColumns = ["id"],
            childColumns = ["dateId"],
            onDelete = ForeignKey.CASCADE
        )]
)
data class CalendarEntity(
    @PrimaryKey(autoGenerate = true) val id: Long,
    val date: String,
    val day: Int,
    val format: String,
    @ColumnInfo(name = "dateId") val dateId: Long,
    val type: CalendarType,
    @Embedded(prefix = "weekday_") val weekday: WeekdayEntity,
    @Embedded(prefix = "month_") val month: MonthEntity,
    val holidays: List<String>?
) {
    companion object {
        const val TABLE_NAME = "Calendar"
    }
}