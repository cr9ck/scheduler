package com.andrew.schedule.model.mapper

import com.andrew.schedule.model.data.Month
import com.andrew.schedule.model.db.entity.MonthEntity
import javax.inject.Inject

class MonthMapper @Inject constructor(): Mapper<Month, MonthEntity> {

    override fun mapToModel(entity: MonthEntity) = with(entity) { Month(number, en, ar) }

    override fun mapToEntity(model: Month) = with(model) { MonthEntity(number, en, ar) }

    override fun mapToModel(entity: List<MonthEntity>) = entity.map { mapToModel(it) }

    override fun mapToEntity(model: List<Month>) = model.map { mapToEntity(it) }
}