package com.andrew.schedule.model.data

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class DataList(
    @SerializedName(value = "data") val data: List<Data>
): Serializable