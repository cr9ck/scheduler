package com.andrew.schedule.model.data

import com.google.gson.annotations.SerializedName

data class Meta(
    @SerializedName(value = "latitude") val latitude: Double,
    @SerializedName(value = "longitude") val longitude: Double,
    @SerializedName(value = "timezone") val timezone: String
)