package com.andrew.schedule.model

import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.location.Location
import androidx.core.content.edit
import com.andrew.schedule.R
import javax.inject.Inject

class AppPreference @Inject constructor(private val context: Context) {

    companion object {
        private val PREFERENCE_NAME = "AppPreference"
        private val PREFERENCE_KEY_MONTH = "PREFERENCE_KEY_MONTH"
        private val PREFERENCE_KEY_LATITUDE = "PREFERENCE_KEY_LATITUDE"
        private val PREFERENCE_KEY_LONGITUDE = "PREFERENCE_KEY_LONGITUDE"
        private val PREFERENCE_THEME = "PREFERENCE_THEME"
        val CACHED_DISTANCE: Double = 50.0
    }

    private val preferences = context.getSharedPreferences(PREFERENCE_NAME, MODE_PRIVATE)

    fun isLocationCached(location: Location): Boolean {
        return getCachedLocation()?.let {
            location.distanceTo(it) < CACHED_DISTANCE
        } ?: false
    }

    fun putCachedLocation(location: Location) {
        preferences.edit {
            putString(PREFERENCE_KEY_LATITUDE, location.latitude.toString())
            putString(PREFERENCE_KEY_LONGITUDE, location.longitude.toString())
        }
    }

    fun getCachedLocation(): Location? {
        val latitudeCached =
            preferences.getString(PREFERENCE_KEY_LATITUDE, null)?.toDouble() ?: return null
        val longitudeCached =
            preferences.getString(PREFERENCE_KEY_LONGITUDE, null)?.toDouble() ?: return null
        return Location("cached").apply {
            latitude = latitudeCached
            longitude = longitudeCached
        }
    }

    fun putCachedMonth(month: Int) {
        preferences.edit {
            putInt(PREFERENCE_KEY_MONTH, month)
        }
    }

    fun getCachedMonth() = preferences.getInt(PREFERENCE_KEY_MONTH, -1)

    fun getTheme() =
        preferences.getString(PREFERENCE_THEME, context.getString(R.string.theme_light))

    fun putTheme(theme: Int) = preferences.edit {
        putString(PREFERENCE_THEME, context.getString(theme))
    }
}