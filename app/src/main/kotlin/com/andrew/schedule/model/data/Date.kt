package com.andrew.schedule.model.data

import com.google.gson.annotations.SerializedName

data class Date(
    @SerializedName(value = "readable") val readable: String,
    @SerializedName(value = "timestamp") val timestamp: Long,
    @SerializedName(value = "gregorian") val gregorian: CalendarModel,
    @SerializedName(value = "hijri") val hijri: CalendarModel
)