package com.andrew.schedule.model.mapper

interface Mapper<M, E> {

    fun mapToModel(entity: E): M

    fun mapToEntity(model: M): E

    fun mapToModel(entity: List<E>): List<M>

    fun mapToEntity(model: List<M>): List<E>
}