package com.andrew.schedule.model.db.typeConverters

import androidx.room.TypeConverter
import com.andrew.schedule.model.db.entity.TimingEntity
import com.google.gson.Gson


class TimingTypeConverter {

    private val splitter = ", "

    @TypeConverter
    fun toList(timings: String): List<TimingEntity> {
        return timings.split(splitter).map { Gson().fromJson(it.trim(), TimingEntity::class.java) }
    }

    @TypeConverter
    fun toString(timings: List<TimingEntity>): String {
        return timings.joinToString(separator = splitter) { Gson().toJson(it) }
    }
}