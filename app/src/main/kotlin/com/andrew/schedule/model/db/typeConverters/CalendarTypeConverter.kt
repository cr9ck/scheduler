package com.andrew.schedule.model.db.typeConverters

import androidx.room.TypeConverter
import com.andrew.schedule.model.db.entity.CalendarType

class CalendarTypeConverter {

    @TypeConverter
    fun convertToType(int: Int) = CalendarType.values()[int]

    @TypeConverter
    fun convertToInt(calendarType: CalendarType) = calendarType.ordinal
}