package com.andrew.schedule.model.repository

import com.andrew.schedule.model.data.Data
import com.andrew.schedule.model.data.DataList
import com.andrew.schedule.network.PrayerApi
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LocationRepository(
    private val prayerApi: PrayerApi
) {

    suspend fun sendRequest(
        onSuccessCallback: (List<Data>) -> Unit,
        onErrorCallback: (Throwable) -> Unit,
        latitude: String,
        longitude: String,
        month: Int,
        year: Int) {

        prayerApi.getDataByLocation(
            latitude,
            longitude,
            month,
            year
        )
            .enqueue(object : Callback<DataList> {
                override fun onFailure(call: Call<DataList>, t: Throwable) {
                    t.printStackTrace()
                    onErrorCallback(t)
                }

                override fun onResponse(call: Call<DataList>, response: Response<DataList>) {
                    response.body()?.let { onSuccessCallback(it.data) }
                }
            })
    }
}