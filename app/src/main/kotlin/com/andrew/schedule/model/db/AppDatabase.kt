package com.andrew.schedule.model.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.andrew.schedule.model.db.entity.*
import com.andrew.schedule.model.db.typeConverters.CalendarTypeConverter
import com.andrew.schedule.model.db.typeConverters.HolidaysTypeConverter
import com.andrew.schedule.model.db.typeConverters.TimingTypeConverter

@Database(
    entities = [
        CalendarEntity::class,
        DataEntity::class,
        DateEntity::class,
        MetaEntity::class
    ],
    version = AppDatabase.DATABASE_VERSION
)
@TypeConverters(
    TimingTypeConverter::class,
    HolidaysTypeConverter::class,
    CalendarTypeConverter::class
)
abstract class AppDatabase : RoomDatabase() {

    companion object {
        const val DATABASE_NAME = "Scheduler"
        const val DATABASE_VERSION = 1

        private lateinit var instance: AppDatabase
        fun getInstance(context: Context): AppDatabase {
            if (this::instance.isInitialized.not())
                instance = Room.databaseBuilder(
                    context.applicationContext,
                    AppDatabase::class.java,
                    DATABASE_NAME
                ).build()
            return instance
        }
    }

    abstract fun dataDao(): DataDao
}