package com.andrew.schedule.model.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class TimingModel(
    val name: String,
    val time: String
): Parcelable