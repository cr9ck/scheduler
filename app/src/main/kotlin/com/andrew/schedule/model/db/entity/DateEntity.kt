package com.andrew.schedule.model.db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity(tableName = DateEntity.TABLE_NAME,
    foreignKeys = [
        ForeignKey(
            entity = DataEntity::class,
            parentColumns = ["id"],
            childColumns = ["dataId"],
            onDelete = ForeignKey.CASCADE
        )]
)

data class DateEntity(
    @PrimaryKey(autoGenerate = true) val id: Long,
    @ColumnInfo(name = "dataId") val dataId: Long,
    @ColumnInfo(name = "readable") val readable: String,
    @ColumnInfo(name = "timestamp") val timestamp: Long
) {
    companion object {
        const val TABLE_NAME = "Date"
    }
}