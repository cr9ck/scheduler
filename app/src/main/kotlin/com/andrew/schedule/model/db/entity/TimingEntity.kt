package com.andrew.schedule.model.db.entity

data class TimingEntity(
    val name: String,
    val time: String
)