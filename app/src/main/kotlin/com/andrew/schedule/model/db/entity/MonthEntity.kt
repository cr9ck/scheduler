package com.andrew.schedule.model.db.entity

data class MonthEntity(
    val number : Int,
    val en : String,
    val ar : String?
)