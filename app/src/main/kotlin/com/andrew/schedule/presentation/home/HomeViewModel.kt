package com.andrew.schedule.presentation.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.andrew.base.BaseViewModel
import com.andrew.schedule.model.data.CalendarModel
import com.andrew.schedule.model.db.entity.CalendarType
import com.andrew.schedule.model.mapper.MonthMapper
import com.andrew.schedule.model.mapper.WeekdayMapper
import com.andrew.schedule.model.repository.DataRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.channels.BroadcastChannel
import kotlinx.coroutines.channels.ConflatedBroadcastChannel
import kotlinx.coroutines.flow.FlowCollector
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.withContext
import javax.inject.Inject

class HomeViewModel @Inject constructor(
    private val dataRepository: DataRepository,
    private val monthMapper: MonthMapper,
    private val weekdayMapper: WeekdayMapper
) : BaseViewModel() {

    private val selectedType: BroadcastChannel<CalendarType> = ConflatedBroadcastChannel()
    @InternalCoroutinesApi
    private var _calendarList: LiveData<List<CalendarModel>> = getCalendarListLd()
    @InternalCoroutinesApi
    val calendarList: LiveData<List<CalendarModel>> = _calendarList

    var actionEvent: MutableLiveData<Event?> = MutableLiveData()

    init {
        selectedType.offer(CalendarType.GREGORIAN)
    }

    fun onClicked(calendarModel: CalendarModel) {
        doWork {
            val dataId = dataRepository.getDataIdBy(calendarModel.day)
            val navigate = Event.Navigate(dataId, calendarModel.weekday.en)
            withContext(Dispatchers.Main) {
                actionEvent.value = navigate
                actionEvent.value = null
            }

        }
    }

    @InternalCoroutinesApi
    fun getCalendarListLd() =
        MutableLiveData<List<CalendarModel>>().apply {
            doWork {

                selectedType.asFlow()
                    .map { dataRepository.getCalendarBy(it) }
                    .map {
                        it.map { entity ->
                            CalendarModel(
                                entity.date,
                                entity.day,
                                entity.format,
                                weekdayMapper.mapToModel(entity.weekday),
                                monthMapper.mapToModel(entity.month),
                                entity.holidays
                            )
                        }
                    }
                    .collect(object : FlowCollector<List<CalendarModel>> {
                        override suspend fun emit(value: List<CalendarModel>) {
                            postValue(value)
                        }
                    })
            }
        }

    fun setGregorianDate() = selectedType.offer(CalendarType.GREGORIAN)

    fun setHijriDate() = selectedType.offer(CalendarType.HIJRI)

    sealed class Event() {
        class Navigate(val dataId: Long, val weekday: String) : Event()
    }
}