package com.andrew.schedule.presentation.splash

import android.Manifest.permission.ACCESS_COARSE_LOCATION
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.View
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import com.andrew.base.BaseFragment
import com.andrew.base.finish
import com.andrew.base.isPermissionGranted
import com.andrew.schedule.R
import com.andrew.schedule.di.component.AppComponentProvider
import kotlinx.android.synthetic.main.fragment_splash_screen.*

class SplashScreenFragment :
    BaseFragment<SplashViewModel>(SplashViewModel::class.java, R.layout.fragment_splash_screen) {

    private val LOCATION_PERMISSION_REQUEST = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AppComponentProvider.appComponent.inject(this)
        checkLocationPermissionGranted()
    }

    override fun initObservers() {
        viewModel.eventLd.observe(viewLifecycleOwner) {
            when (it) {
                SplashViewModel.Event.DataSaved -> {
                    findNavController().navigate(SplashScreenFragmentDirections.actionToHome())
                    retry.visibility = View.GONE
                }
                SplashViewModel.Event.StartFetching -> {
                    progress.visibility = View.VISIBLE
                    statusText.text = getString(R.string.start_fetching)
                    retry.visibility = View.GONE
                }
                SplashViewModel.Event.SavingData -> {
                    statusText.text = getString(R.string.saving_data)
                    retry.visibility = View.GONE
                }
                SplashViewModel.Event.Error -> {
                    statusText.text = getString(R.string.some_error)
                    retry.visibility = View.VISIBLE
                }
            }
        }
    }

    override fun initListeners() {
        retry.setOnClickListener { checkLocationPermissionGranted() }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            LOCATION_PERMISSION_REQUEST -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    viewModel.receiveLocation()
                } else {
                    finish()
                }
                return
            }
            else -> finish()
        }
    }

    private fun checkLocationPermissionGranted() {
        if (requireContext().isPermissionGranted(ACCESS_COARSE_LOCATION)) {
            viewModel.receiveLocation()
            return
        }
        requestPermissions(
            arrayOf(ACCESS_COARSE_LOCATION),
            LOCATION_PERMISSION_REQUEST
        )
    }
}