package com.andrew.schedule.presentation

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.andrew.schedule.R
import com.andrew.schedule.di.component.AppComponentProvider
import com.andrew.schedule.model.AppPreference
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var appPreference: AppPreference

    override fun onCreate(savedInstanceState: Bundle?) {
        AppComponentProvider.appComponent.inject(this)
        setTheme(getThemeFromPref())
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun toggleTheme() {
        when(appPreference.getTheme()?:getString(R.string.theme_light)) {
            getString(R.string.theme_light) -> appPreference.putTheme(R.string.theme_dark)
            else -> appPreference.putTheme(R.string.theme_light)
        }
        recreate()
    }

    private fun getThemeFromPref(): Int{
        return when (appPreference.getTheme()) {
            getString(R.string.theme_dark) -> {
                appPreference.putTheme(R.string.theme_dark)
                R.style.DarkTheme
            }
            else -> {
                appPreference.putTheme(R.string.theme_light)
                R.style.LightTheme
            }
        }
    }
}
