package com.andrew.schedule.presentation.home

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.andrew.base.colorFromAttr
import com.andrew.schedule.R
import com.andrew.schedule.model.data.CalendarModel
import com.google.android.material.card.MaterialCardView
import kotlinx.android.synthetic.main.item_day.view.*
import java.util.*

class HomeListAdapter(private val onItemClicked: (CalendarModel) -> Unit): RecyclerView.Adapter<HomeListAdapter.ItemView>() {

    private var dataList: List<CalendarModel> = emptyList()

    fun setData(data: List<CalendarModel>) {
        dataList = data
        notifyDataSetChanged()
    }

    fun getCurrentDayPosition() = dataList.indexOf(dataList.find { it.day == Calendar.getInstance().get(Calendar.DAY_OF_MONTH)})

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ItemView(LayoutInflater.from(parent.context).inflate(R.layout.item_day, parent, false))

    override fun getItemCount() = dataList.size

    override fun onBindViewHolder(holder: ItemView, position: Int) = holder.bind(dataList[position])

    inner class ItemView(itemView: View): RecyclerView.ViewHolder(itemView) {

        fun bind(data: CalendarModel) {
            with(itemView) {
                if (data.day == Calendar.getInstance().get(Calendar.DAY_OF_MONTH)) {
                    (itemView as MaterialCardView).strokeColor = context.getColor(android.R.color.holo_red_light)
                } else {
                    (itemView as MaterialCardView).strokeColor = context.colorFromAttr(R.attr.itemDayColor)
                }
                setOnClickListener { onItemClicked(data) }

                itemDate.text = data.day.toString()
                itemMonth.text = data.month.en
                itemYear.text = data.date
            }
        }
    }
}