package com.andrew.schedule.presentation.splash

import android.content.IntentSender
import android.location.Location
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.andrew.base.BaseViewModel
import com.andrew.schedule.model.AppPreference
import com.andrew.schedule.model.data.Data
import com.andrew.schedule.model.repository.DataRepository
import com.andrew.schedule.model.repository.LocationRepository
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.tasks.Task
import java.util.*
import javax.inject.Inject

class SplashViewModel @Inject constructor(
    private val locationRepository: LocationRepository,
    private val dataRepository: DataRepository,
    private var fusedLocationClient: FusedLocationProviderClient,
    private val clientSettings: SettingsClient,
    private val appPreference: AppPreference
) : BaseViewModel() {

    private val _event = MutableLiveData<Event>()
    val eventLd: LiveData<Event> = _event

    fun receiveLocation() {
        doWork {
            val builder = LocationSettingsRequest.Builder()
                .addAllLocationRequests(listOf(createLocationRequest()))
            val task: Task<LocationSettingsResponse> =
                clientSettings.checkLocationSettings(builder.build())
            task
                .addOnFailureListener {
                    _event.value = Event.Error
                    handleLocationSettingsFailure(it)
                }
                .addOnSuccessListener {
                    handleLocationSettingsSuccess()
                }
        }
    }

    private fun handleLocationSettingsFailure(exception: Exception) {
        if (exception is ResolvableApiException) {
            try {
                _event.value = Event.Error
            } catch (sendEx: IntentSender.SendIntentException) {
                _event.value = Event.Error
                sendEx.printStackTrace()
            }
        }
    }

    private fun handleLocationSettingsSuccess() =
        fusedLocationClient.lastLocation
            .addOnSuccessListener {
                it?.let {
                    if (
                        appPreference.isLocationCached(Location("received").apply {
                            latitude = it.latitude
                            longitude = it.longitude
                        })
                    ) {
                        checkCachedMonthData()
                    } else {
                        sendRequest()
                    }
                } ?: run {
                    _event.value = Event.Error
                }
            }
            .addOnFailureListener {
                _event.value = Event.Error
            }

    private fun checkCachedMonthData() {
        if (appPreference.getCachedMonth() == Calendar.getInstance().get(Calendar.MONTH) + 1) {
            _event.value = Event.DataSaved
        } else {
            sendRequest()
        }
    }

    private fun sendRequest() {
        fusedLocationClient.lastLocation.addOnSuccessListener {
            _event.value = Event.StartFetching
            doWork {
                it.accuracy
                it.bearing
                locationRepository.sendRequest(
                    this@SplashViewModel::insertData,
                    this@SplashViewModel::onError,
                    it.latitude.toString(),
                    it.longitude.toString(),
                    Calendar.getInstance().get(Calendar.MONTH) + 1,
                    Calendar.getInstance().get(Calendar.YEAR)
                )
            }
        }
    }

    private fun insertData(dataList: List<Data>) {
        _event.value = Event.SavingData
        doWork {
            val meta = dataList.first().meta
            dataRepository.insertData(dataList)
            appPreference.putCachedLocation(Location("cached").apply {
                latitude = meta.latitude
                longitude = meta.longitude
            })
            appPreference.putCachedMonth(Calendar.getInstance().get(Calendar.MONTH) + 1)
            _event.postValue(Event.DataSaved)
        }
    }

    private fun onError(throwable: Throwable) {
        throwable.printStackTrace()
    }

    private fun createLocationRequest() = LocationRequest.create().apply {
        interval = 10_000
        fastestInterval = 5_000
        priority = LocationRequest.PRIORITY_LOW_POWER
    }

    enum class Event {
        StartFetching,
        SavingData,
        DataSaved,
        Error
    }
}