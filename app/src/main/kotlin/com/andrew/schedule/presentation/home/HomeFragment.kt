package com.andrew.schedule.presentation.home

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.andrew.base.BaseFragment
import com.andrew.schedule.R
import com.andrew.schedule.di.component.AppComponentProvider
import com.andrew.schedule.model.data.CalendarModel
import com.andrew.schedule.presentation.MainActivity
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.coroutines.InternalCoroutinesApi

@InternalCoroutinesApi
class HomeFragment :
    BaseFragment<HomeViewModel>(HomeViewModel::class.java, R.layout.fragment_home) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AppComponentProvider.appComponent.inject(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initRecyclerView()
        super.onViewCreated(view, savedInstanceState)
    }

    override fun initObservers() {
        viewModel.calendarList.observe(viewLifecycleOwner, this::setData)
        viewModel.actionEvent.observe(viewLifecycleOwner, ::handleEvent)

    }

    override fun initToolbar() {
        toolbar.inflateMenu(R.menu.home_menu)
        toolbar.setOnMenuItemClickListener { onToolbarItemClicked(it) }
    }

    private fun setData(data: List<CalendarModel>) {
        (list.adapter as HomeListAdapter).setData(data)
        list.layoutManager?.let { it.scrollToPosition((list.adapter as HomeListAdapter).getCurrentDayPosition()) }
    }

    private fun handleEvent(event: HomeViewModel.Event?) {
        when(event) {
            is HomeViewModel.Event.Navigate -> findNavController().navigate(HomeFragmentDirections.actionToTiming(event.dataId, event.weekday))
        }
    }

    private fun initRecyclerView() {
        with(list) {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = HomeListAdapter(this@HomeFragment::onItemClicked)
        }
    }

    private fun onItemClicked(calendarModel: CalendarModel) = viewModel.onClicked(calendarModel)

    private fun onToolbarItemClicked(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.gregorian -> viewModel.setGregorianDate()
            R.id.hijri -> viewModel.setHijriDate()
            R.id.theme -> (requireActivity() as MainActivity).toggleTheme()
        }
        return true
    }
}