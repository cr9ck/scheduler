package com.andrew.schedule.presentation.timing

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.andrew.schedule.R
import com.andrew.schedule.model.data.TimingModel
import kotlinx.android.synthetic.main.item_pray.view.*

class TimingListAdapter: RecyclerView.Adapter<TimingListAdapter.ViewHolder>() {

    private var dataList: List<TimingModel> = emptyList()

    fun setData(data: List<TimingModel>) {
        dataList = data
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_pray, parent, false))

    override fun getItemCount() = dataList.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(dataList[position])

    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {

        fun bind(timingModel: TimingModel) {
            with(itemView) {
                name.text = timingModel.name
                time.text = timingModel.time
            }
        }
    }
}