package com.andrew.schedule.presentation.timing

import androidx.lifecycle.MutableLiveData
import com.andrew.base.BaseViewModel
import com.andrew.schedule.model.data.TimingModel
import com.andrew.schedule.model.repository.DataRepository
import javax.inject.Inject

class TimingViewModel @Inject constructor(private val dataRepository: DataRepository): BaseViewModel() {

    private var _dataList: MutableLiveData<List<TimingModel>> = MutableLiveData()
    val dataList = _dataList

    fun initData(dataId: Long) {
        doWork {
            _dataList.postValue(
                dataRepository.getTimingByDataId(dataId)
                    .map { TimingModel(it.name, it.time) }
            )
        }
    }
}