package com.andrew.schedule.presentation.timing

import android.os.Bundle
import android.view.View
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.andrew.base.BaseFragment
import com.andrew.schedule.R
import com.andrew.schedule.di.component.AppComponentProvider
import kotlinx.android.synthetic.main.fragment_home.*

class TimingFragment : BaseFragment<TimingViewModel>(TimingViewModel::class.java, R.layout.fragment_timing) {

    private val args: TimingFragmentArgs by navArgs()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AppComponentProvider.appComponent.inject(this)
        viewModel.initData(args.dataId)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        iniRecyclerView()
        super.onViewCreated(view, savedInstanceState)
    }

    override fun initToolbar() {
        toolbar.title = args.weekday
        toolbar.setNavigationIcon(R.drawable.ic_back)
        toolbar.setNavigationOnClickListener { findNavController().popBackStack() }
    }

    override fun initObservers() {
        viewModel.dataList.observe(viewLifecycleOwner, (list.adapter as TimingListAdapter)::setData)
    }

    private fun iniRecyclerView() {
        with(list) {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = TimingListAdapter().apply { }//setData(args.timings.data) }
        }
    }
}