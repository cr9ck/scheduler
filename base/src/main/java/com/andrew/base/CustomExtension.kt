package com.andrew.base

import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.util.TypedValue
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer

inline fun <L> LiveData<L?>.observe(viewLifecycleOwner: LifecycleOwner, crossinline onUpdate: (L?) -> Unit) {
    this.observe(viewLifecycleOwner, Observer { it?.let { onUpdate(it) } })
}

fun Context.isPermissionGranted(permission: String) = ContextCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED

//fun Fragment.requestPermission(permission: String, requestCode: Int = 0) {
//    requestPermissions(arrayOf(permission), requestCode)
//}
fun Activity.shouldShowRequestPermissionRationale(permission: String) {
    ActivityCompat.shouldShowRequestPermissionRationale(this, permission)
}

fun Fragment.finish() = requireActivity().finish()

fun Context.colorFromAttr(attr: Int): Int {
    val typedValue = TypedValue()
    val theme = theme
    theme.resolveAttribute(attr, typedValue, true)
    return typedValue.data
}