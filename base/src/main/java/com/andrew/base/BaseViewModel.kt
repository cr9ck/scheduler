package com.andrew.base

import android.content.Context
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.*
import javax.inject.Inject

abstract class BaseViewModel : ViewModel() {

    private var viewModelJob = Job()
    private var viewModelScope = CoroutineScope(Dispatchers.Main + viewModelJob)

    @Inject
    protected lateinit var context: Context

    fun <R> doWork(work: suspend CoroutineScope.() -> R) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                work.invoke(this)
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }
}